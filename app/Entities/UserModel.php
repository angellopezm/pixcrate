<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *    required={"email", "username", "password"},
 *
 *    @SWG\Property(
 *        property="email",
 *        type="string",
 *        description="User's email",
 *        example="example@example.com"
 *    ),
 *    @SWG\Property(
 *        property="username",
 *        type="string",
 *        description="User's username",
 *        example="examplename"
 *    ),
 *    @SWG\Property(
 *        property="password",
 *        type="string",
 *        description="User's password",
 *        example="examplepassword"
 *    ),
 * )
 */

final class UserModel extends Model
{

    // User model for user table

    protected $table = 'user';

    protected $primaryKey = 'email';

    protected $fillable = ['email', 'username', 'password'];

    public $timestamps = false;

    public $incrementing = false;

}
