-- MySQL Script generated by MySQL Workbench
-- Tue Apr 10 13:19:03 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema snowflake_coponieve_742k18
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `snowflake_coponieve_742k18` ;

-- -----------------------------------------------------
-- Schema snowflake_coponieve_742k18
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `snowflake_coponieve_742k18` DEFAULT CHARACTER SET utf8 ;
USE `snowflake_coponieve_742k18` ;

-- -----------------------------------------------------
-- Table `snowflake_coponieve_742k18`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `snowflake_coponieve_742k18`.`user` ;

CREATE TABLE IF NOT EXISTS `snowflake_coponieve_742k18`.`user` (
  `email` VARCHAR(30) NOT NULL,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `firstName` VARCHAR(20) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `right` ENUM('NORMAL', 'ADMIN', 'BANNED') NOT NULL,
  PRIMARY KEY (`email`, `username`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
