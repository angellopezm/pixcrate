<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// ##################### USER's ENDPOINTS #####################
Route::post("users", "UserController@create");
Route::get("users/{filter}/{value}", "UserController@loadByFilter");
Route::post("users/user", "UserController@load");

// ##################### AUTH's ENDPOINTS #####################
Route::get("auth/{filter}/{value}", "AuthController@verify");