<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * @SWG\POST(
     *   path="/users",
     *   summary="Create a new user if doesn't exist",
     *   operationId="user.create",
     *   tags={"users"},
     * 
     *   @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/UserModel"),
     *   ),
     * 
     *   @SWG\Response(
     *     response=201,
     *     description="Resource created",
     *   ),
     *   @SWG\Response(
     *     response="409",
     *     description="Resource couldn't be created due to a conflict in the request: User already exists"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description=""
     *   ),
     *   
     * )
     */
    public function create(Request $request)
    {
        Log::info("Entra");
        return UserService::createUser($request->getContent());
    }

    /**
     * @SWG\POST(
     *   path="/users/user",
     *   summary="Retrieves an user, giving a complete user model json",
     *   operationId="user.get",
     *   tags={"users"},
     * 
     *   @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/UserModel"),
     *   ),
     * 
     *   @SWG\Response(
     *     response=200,
     *     description="Resource found",
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="Resource not found: User doesn't exist"
     *   )
     * )
     */
    public function load(Request $request)
    {
        //return UserService::loadUser($request->getContent());
    }

    /**
     * @SWG\GET(
     *   path="/users/{filter}/{value}",
     *   summary="Retrieves an user, giving an attribute to filter for and it's value",
     *   operationId="user.get.byFilter",
     *   tags={"users"},
     * 
     *   @SWG\Parameter(
     *     name="filter",
     *     type="array",
     *     in="path",
     *     @SWG\Items(
     *         type="string",
     *         enum={"email", "username"},
     *     ),
     *     collectionFormat="multi",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="value",
     *     type="string",
     *     in="path",
     *     required=true,
     *   ),
     * 
     *   @SWG\Response(
     *     response=200,
     *     description="Resource found",
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="Resource not found: User doesn't exist"
     *   )
     * )
     */
    public function loadByFilter(Request $request)
    {
        return UserService::loadUserByFilter($request->route()->parameters());
    }

}
