<?php

namespace Tests\Feature;

use App\Entities\UserModel;
use App\Services\UserService;
use Tests\TestCase;

class UserServiceTest extends TestCase
{

    /**
     * Create given user if doesn't exist.
     *
     * @return void
     */

    public function test_CreateIfDoesntExist()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::createUser($user);

        $this->assertTrue($response);
    }

    /**
     * Load given user if already exists.
     *
     * Test result should be passed.
     *
     * @return void
     */

    public function test_LoadIfExists_OK()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::loadUser($user);

        $this->assertTrue($response);
    }

    /**
     * Update given user if already exists.
     *
     * Test result should be passed.
     *
     * @return void
     */

    public function test_UpdateIfExists_OK()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::updateUser($user, array("username" => "ejemplito"));

        $this->assertTrue($response);
    }

    /**
     * Delete given user if already exists.
     *
     * Test result should be passed.
     *
     * @return void
     */

    public function test_DeleteIfExists_OK()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::deleteUser($user);

        $this->assertTrue($response);
    }

    /**
     * Load given user if already exists.
     *
     * Test result should be failed.
     *
     * @return void
     */

    public function test_LoadIfExists_WrongMail_KO()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::loadUser($user);

        $this->assertTrue($response);
    }

    /**
     * Update given user if already exists.
     *
     * Test result should be failed.
     *
     * @return void
     */

    public function test_UpdateIfExists_WrongMail_KO()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "ejemplo@examplemail.com", "username" => "example"));
        $response = UserService::updateUser($user, array("username" => "ejemplito"));

        $this->assertTrue($response);
    }

    /**
     * Delete given user if already exists.
     *
     * Test result should be failed.
     *
     * @return void
     */

    public function test_DeleteIfExists_WrongMail_KO()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "ejemplo@examplemail.com", "username" => "example"));
        $response = UserService::deleteUser($user);

        $this->assertTrue($response);
    }

    /**
     * Load given user if already exists.
     *
     * Test result should be failed.
     *
     * @return void
     */

    public function test_LoadIfExists_WrongUsername_KO()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::loadUser($user);

        $this->assertTrue($response);
    }

    /**
     * Update given user if already exists.
     *
     * Test result should be failed.
     *
     * @return void
     */

    public function test_UpdateIfExists_WrongUsername_KO()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::updateUser($user, array("username" => "ejemplito"));

        $this->assertTrue($response);
    }

    /**
     * Delete given user if already exists.
     *
     * Test result should be failed.
     *
     * @return void
     */

    public function test_DeleteIfExists_WrongUsername_KO()
    {
        //$this->markTestSkipped('must be revisited.');
        $user = new UserModel(array("email" => "example@examplemail.com", "username" => "example"));
        $response = UserService::deleteUser($user);

        $this->assertTrue($response);
    }



}
