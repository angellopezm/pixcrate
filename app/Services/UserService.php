<?php

namespace App\Services;

use App\Entities\UserModel;
use Illuminate\Support\Facades\Log;

final class UserService
{

    public static function createUser($httpContent = "")
    {
        $decodedAsArray = json_decode($httpContent, true);
        $user = new UserModel($decodedAsArray);
        
        $count = $user::where("email", "=", $user->email, "or", "username", "=", $user->username)->count();
        if ($count == 0) {
            $user->save();
            return response()->json(["status" => "Resource created"], 201);         
        } else {
            return response()->json(["status" => "Resource couldn't be created due to a conflict in the request: User already exists"], 409);
        }
    }

    public static function loadUser($httpContent = "")
    {
        /*
        $decodedAsArray = json_decode($httpContent, true);
        $user = new UserModel($decodedAsArray);

        $count = $user::where($httpContent["filter"], "=", $httpContent["value"])->count();
        if ($count == 1) {
            return response()->json($user->first(), 200, [], JSON_UNESCAPED_UNICODE);    
        } else {
            return response()->json(["status" => "Resource not found: User doesn't exist"], 404);   
        }
        */
    }

    public static function loadUserByFilter($httpContent = "")
    {
        $user = new UserModel([$httpContent["filter"] => $httpContent["value"]]);
        $count = $user::where($httpContent["filter"], "=", $httpContent["value"])->count();
        if ($count == 1) {
            return response()->json($user->first(), 200, [], JSON_UNESCAPED_UNICODE);    
        } else {
            return response()->json(["status" => "Resource not found: User doesn't exist"], 404);   
        }
        
    }

    public static function updateUser($user = UserModel::class, $values = [])
    {
        $count = $user::where("email", "=", $user->email, "or", "username", "=", $user->username)->count();
        if ($count == 1) {
            $user::where("email", "=", $user->email, "or", "username", "=", $user->username)->update($values);
            return true;
        } else {
            Log::warning("This user doesn't exist");
            return false;
        }
    }

    public static function deleteUser($user = UserModel::class)
    {
        $count = $user::where("email", "=", $user->email, "or", "username", "=", $user->username)->count();
        if ($count == 1) {
            $user::where("email", "=", $user->email, "or", "username", "=", $user->username)->delete();
            return true;
        } else {
            Log::warning("This user doesn't exist");
            return false;
        }
    }

}
