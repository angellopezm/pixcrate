<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AuthService;

class AuthController extends Controller
{
    /**
     * @SWG\GET(
     *   path="/auth/{filter}/{value}",
     *   summary="Retrieves an user, giving an attribute to filter for and it's value",
     *   operationId="auth.grant.byFilter",
     *   tags={"auth"},
     * 
     *   @SWG\Parameter(
     *     name="filter",
     *     type="array",
     *     in="path",
     *     @SWG\Items(
     *         type="string",
     *         enum={"email", "username"},
     *     ),
     *     collectionFormat="multi",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="value",
     *     type="string",
     *     in="path",
     *     required=true,
     *   ),
     * 
     *   @SWG\Parameter(
     *     name="password",
     *     type="string",
     *     in="query",
     *     required=false,
     *   ),
     * 
     *   @SWG\Response(
     *     response=200,
     *     description="Resource found",
     *   ),
     *   @SWG\Response(
     *     response="404",
     *     description="Resource not found: User doesn't exist"
     *   )
     * )
     */
    public function verify(Request $request){
        return AuthService::verifyUser([$request->route()->parameters(), $request->query()]);
    }

}
