<?php

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     schemes={"http", "https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Pixcrate API",
 *         description="This is the official Pixcrate API",
 *     ),
 * )
 */

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="auth",
 *   type="basic",
 *   flow="password",
 *   scopes={}
 * )
 */